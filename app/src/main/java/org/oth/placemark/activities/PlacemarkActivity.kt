package org.oth.placemark.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_placemark.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import org.oth.placemark.R
import org.oth.placemark.helpers.readImage
import org.oth.placemark.helpers.readImageFromPath
import org.oth.placemark.helpers.showImagePicker
import org.oth.placemark.main.MainApp
import org.oth.placemark.models.Location
import org.oth.placemark.models.PlacemarkModel

class PlacemarkActivity : AppCompatActivity(), AnkoLogger {

    val IMAGE_REQUEST = 1
    val LOCATION_REQUEST = 2
    var placemark = PlacemarkModel()
    lateinit var app : MainApp
    var editMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_placemark)
        app = application as MainApp

        toolbarAdd.title = title
        setSupportActionBar(toolbarAdd)

        if (intent.hasExtra("placemark_edit")) {
            placemark = intent.extras.getParcelable<PlacemarkModel>("placemark_edit")

            placemarkTitle.setText(placemark.title)
            placemarkDescription.setText(placemark.description)

            if (placemark.image.isNotEmpty()) {
                placemarkImage.setImageBitmap(readImageFromPath(this, placemark.image))
                btnChooseImage.setText(R.string.button_changeImage)
            }

            btnAdd.setText(R.string.button_savePlacemark)
            editMode = true
        }

        btnChooseImage.setOnClickListener {
            info ("Selecting image")
            showImagePicker(this, IMAGE_REQUEST)
        }

        placemarkLocation.setOnClickListener {
            info ("Selecting location")
            var location = Location(41.385352, 2.170218, 15f)

            if (placemark.zoom != 0f) {
                location.lat =  placemark.lat
                location.lng = placemark.lng
                location.zoom = placemark.zoom
            }

            startActivityForResult(intentFor<MapsActivity>().putExtra("location", location), LOCATION_REQUEST)
        }

        btnAdd.setOnClickListener() {
            placemark.title = placemarkTitle.text.toString()
            placemark.description = placemarkDescription.text.toString()

            if (placemark.title.isNotEmpty()) {
                if(editMode) {
                    app.placemarks.update(placemark.copy())
                    info("Save button pressed: $placemark")
                } else {
                    app.placemarks.create(placemark.copy())
                    info("Add button pressed: $placemark")
                }
                setResult(AppCompatActivity.RESULT_OK)
                finish()
            }
            else {
                toast (R.string.errorMessage_addPlacemark)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_placemark, menu)
        if (editMode && menu != null) {
            menu.getItem(0).setVisible(true)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.item_delete -> {
                app.placemarks.delete(placemark)
                finish()
            }
            R.id.item_cancel -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            IMAGE_REQUEST -> {
                if (data != null) {
                    placemark.image = data.getData().toString()
                    placemarkImage.setImageBitmap(readImage(this, resultCode, data))
                    btnChooseImage.setText(R.string.button_changeImage)
                }
            }
            LOCATION_REQUEST -> {
                if (data != null) {
                    val location = data.extras.getParcelable<Location>("location")
                    placemark.lat = location.lat
                    placemark.lng = location.lng
                    placemark.zoom = location.zoom
                }
            }
        }
    }

}
