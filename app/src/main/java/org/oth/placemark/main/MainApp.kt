package org.oth.placemark.main

import android.app.Application
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import org.oth.placemark.models.PlacemarkJSONStore
import org.oth.placemark.models.PlacemarkMemStore
import org.oth.placemark.models.PlacemarkStore

class MainApp : Application(), AnkoLogger {

    lateinit var placemarks: PlacemarkStore

    override fun onCreate() {
        super.onCreate()
        // placemarks = PlacemarkMemStore()
        placemarks = PlacemarkJSONStore(applicationContext)
        info("Placemark started")
    }

}